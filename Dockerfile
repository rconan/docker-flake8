FROM python:3.12.3-alpine3.18@sha256:24680ddf8422899b24756d62b31eb5de782fbb42e9c2bb1c70f1f55fcf891721

COPY poetry.lock pyproject.toml /

ENV PATH "/root/.local/bin:${PATH}"
ARG poetry_installer_version=9b64f71d730d7be00e204b96a095b38af9e909e3
ARG poetry_version=1.7.1
RUN wget -q -O - https://raw.githubusercontent.com/python-poetry/install.python-poetry.org/${poetry_installer_version}/install-poetry.py | python3 - --version ${poetry_version} \
 && poetry config virtualenvs.create false \
 && poetry install --no-root

WORKDIR /code
ENTRYPOINT ["flake8"]
